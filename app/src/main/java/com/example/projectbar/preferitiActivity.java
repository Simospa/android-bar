package com.example.projectbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;

import java.util.ArrayList;

public class preferitiActivity extends AppCompatActivity {
    public GridLayoutManager mGridLayoutManager;
    public RecyclerView mRecyclerView;
    private RecyclerView.Adapter<RecyclerAdapter.Holder> mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferiti);
        Toolbar tool = findViewById(R.id.toolbar);
        tool.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ArrayList<Prodotti> listaPreferiti = MainActivity.dbInstance.getAllItems();
        mGridLayoutManager = new GridLayoutManager(this,2);
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerViewPreferiti);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mAdapter = new RecyclerAdapter(this,listaPreferiti);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
