package com.example.projectbar;
import android.graphics.Bitmap;

public class Prodotti {
    private String nome,descrizione,urlImage;
    private Bitmap mImg;
    private Long ID;
    private Long prezzo,calorie,grad;
    Prodotti(String nome, String descrizione,String urlImage,Long prezzo,Long calorie,Long grad){
        this.nome = nome;
        this.descrizione =descrizione;
        this.urlImage = urlImage;
        this.prezzo = prezzo;
        this.calorie = calorie;
        this.grad = grad;
    }
    public String getNome(){return nome;}
    public String getDescrizione(){return descrizione;}
    public String getImage(){
       return urlImage;
    }
    public synchronized void setBitmap(Bitmap b){mImg = b;}
    public synchronized Bitmap getBitmap(){ return mImg;}
    public void setDbID(Long ID){this.ID = ID;}
    public Long getPrezzo() {
        return prezzo;
    }

    public Long getCalorie() {
        return calorie;
    }

    public Long getGrad() {
        return grad;
    }

    public String[] getArrayToPass(){
        return new String[]{nome,descrizione,urlImage,Long.toString(prezzo),Long.toString(calorie),Long.toString(grad)};
    }

    public Long getID() {
        return ID;
    }
}
