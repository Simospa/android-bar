package com.example.projectbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;


public class registrationActivity extends AppCompatActivity {
    protected EditText emailEditText;
    protected EditText passwordEditText;
    protected Button signUpButton;
    private FirebaseAuth mFirebaseAuth;
    private static final int RC_SIGN_IN = 9001;
    private boolean signInGoogle,singInFacebook;
    private CallbackManager callbackManager;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Toolbar tool = findViewById(R.id.toolbar);
        tool.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mFirebaseAuth = FirebaseAuth.getInstance();

        passwordEditText = (EditText) findViewById(R.id.PasswordEditTextRes);
        emailEditText = (EditText) findViewById(R.id.editTextRegistrationRes);
        signUpButton = (Button) findViewById(R.id.ButtonRegistrationRes);
        signInGoogle = false;
        singInFacebook = false;
        spinner = (ProgressBar)findViewById(R.id.progressBarRegistration);
        spinner.setVisibility(View.GONE);
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("SUCCESSO", "facebook:onSuccess:" + loginResult);
                spinner.setVisibility(View.VISIBLE);
                signUpButton.setEnabled(false);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }
            @Override
            public void onCancel() {
                Log.d("CANCEL", "facebook:onError");
            }
            @Override
            public void onError(FacebookException error) {
                Log.d("ERROR", "facebook:onError", error);
            }
        });
}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickRegistration(View v){
        String password = passwordEditText.getText().toString();
        String email = emailEditText.getText().toString();
        password = password.trim();
        email = email.trim();

        if(email.equals("")|| password.equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(registrationActivity.this);
            builder.setMessage("INSERIRE EMAIL E PASSWORD")
                    .setTitle("ERRORE")
                    .setPositiveButton(android.R.string.ok,null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            spinner.setVisibility(View.VISIBLE);
            signUpButton.setEnabled(false);
            mFirebaseAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(registrationActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d("Entro","Entro in createUserRes");
                            if(task.isSuccessful()){
                                Toast.makeText(getBaseContext(),"Ti sei registrato con successo",Toast.LENGTH_LONG).show();
                                spinner.setVisibility(View.GONE);
                                signUpButton.setEnabled(true);
                                startMainActivity();
                            }else{
                                Log.d("Registration","ERRORE");
                                Toast.makeText(getBaseContext(),"Non ti sei registrato con successo",Toast.LENGTH_LONG).show();
                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(registrationActivity.this);
                                builder.setMessage(task.getException().getMessage())
                                        .setTitle("ERRORE")
                                        .setPositiveButton(android.R.string.ok,null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
        }
    }

    public void signInWithGoogle(View v){
        signInGoogle = true;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(signInGoogle){
            if (requestCode == RC_SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    Log.d("Successo", "firebaseAuthWithGoogle:" + account.getId());
                    spinner.setVisibility(View.VISIBLE);
                    signUpButton.setEnabled(false);
                    firebaseAuthWithGoogle(account.getIdToken());
                } catch (ApiException e) {
                    Log.w("Errore", "Google sign in failed", e);
                }
                finally{
                    signInGoogle = false;
                }
            }
        }
        else if (singInFacebook){
            callbackManager.onActivityResult(requestCode, resultCode, data);
            singInFacebook = false;
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("SUCCES", "signInWithCredential:success");
                            spinner.setVisibility(View.GONE);
                            signUpButton.setEnabled(true);
                            startMainActivity();
                        } else {
                            Log.w("FAILED", "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }

    public void signInFacebook(View v){
        singInFacebook = true;
        Log.d("SIGNINFACEBOOK","TRUE");
    }

    private void startMainActivity(){
        Intent registrationIntent = new Intent(registrationActivity.this, MainActivity.class);
        registrationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        registrationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(registrationIntent);
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("SUCCESS", "signInWithCredential:success");
                            spinner.setVisibility(View.GONE);
                            signUpButton.setEnabled(true);
                            startMainActivity();
                        } else {
                            Log.w("ERROR", "signInWithCredential:failure", task.getException());
                            Toast.makeText(registrationActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
