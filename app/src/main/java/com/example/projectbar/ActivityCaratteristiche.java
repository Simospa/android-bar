package com.example.projectbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Map;

public class ActivityCaratteristiche extends AppCompatActivity {
    private TextView mTextView,textViewPrezzo,textViewCalorie,textViewGrad;
    private CustomViewCarat customPrezzo,customCalorie,customGrad;
    private ImageView image;
    private Button addButton;
    private Prodotti mProd;
    private static final int MAX_PREZZO = 35;
    private static final int MAX_CALORIE = 900;
    private static final int MAX_GRAD = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caratteristiche);
        Toolbar tool = findViewById(R.id.toolbar);
        tool.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mTextView = (TextView)findViewById(R.id.carat_text_view);
        Bundle b = getIntent().getExtras();
        image = (ImageView)findViewById(R.id.carat_image);
        addButton = (Button)findViewById(R.id.add_preferiti_button);
        addButton.setVisibility(View.GONE);
        textViewPrezzo = (TextView)findViewById(R.id.textViewPrezzo);
        textViewCalorie = (TextView)findViewById(R.id.textViewCalorie);
        textViewGrad = (TextView)findViewById(R.id.textViewGrad);
        customPrezzo = (CustomViewCarat)findViewById(R.id.CustomViewCarat3);
        customCalorie = (CustomViewCarat)findViewById(R.id.CustomViewCarat2);
        customGrad = (CustomViewCarat)findViewById(R.id.CustomViewCarat);
        mProd = null;
        if(b.getString("barcode")!=null){
            searchOnDB(b.getString("barcode"));
        }
        else{
            String[] values = b.getStringArray("RecyclerView");
            assert values != null;
            Prodotti p = new Prodotti(values[0],values[1],values[2],Long.parseLong(values[3]),Long.parseLong(values[4]),Long.parseLong(values[5]));
            mTextView.setText(p.getNome());
            image.setImageBitmap(RecyclerAdapter.onClickBitmap);
            populateLayout(p);
            mProd = p;
            addButton.setVisibility(View.VISIBLE);
            addButton.setText(MainActivity.dbInstance.checkIfExists(p) ? "Rimuovi":"SALVA");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void searchOnDB(String barcode){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Barcodes").child(barcode);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.getValue()!=null){
                    String name = snapshot.getValue(String.class);
                    String[] arr = name.split("-");
                    DatabaseReference innerRef = FirebaseDatabase.getInstance().getReference(arr[0]).child(arr[1]);

                    innerRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            Map<String,Object> val = (Map<String, Object>) snapshot.getValue();
                            mTextView.setText((String)val.get("Titolo"));
                            Long prezzo,cal,grad;
                            prezzo = (Long)val.get("Prezzo");
                            cal = (Long)val.get("Calorie");
                            grad = (Long)val.get("Gradazione");
                            mProd = new Prodotti((String)val.get("Titolo"),(String)val.get("Descrizione"),(String)val.get("imgId"),prezzo,cal,grad);
                            populateLayout(mProd);
                            addButton.setVisibility(View.VISIBLE);
                            addButton.setText(MainActivity.dbInstance.checkIfExists(mProd) ? "Togli dai preferiti":"SALVA");
                            downloadImage();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                        }
                    });
                }
                else{
                    mTextView.setText("Nessun articolo associato a questo codice");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    private void downloadImage(){
        if(mProd!=null){
            StorageReference storage = FirebaseStorage.getInstance().getReference();
            final long ONE_MEGABYTE = 1024*1024;
            StorageReference storageRef = storage.child(mProd.getImage());
            storageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitMap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                    image.setImageBitmap(bitMap);
                    mProd.setBitmap(bitMap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {}
            });
        }
    }

    private void populateLayout(Prodotti mProd){
        textViewPrezzo.setText(textViewPrezzo.getText()+" "+mProd.getPrezzo()+"€");
        textViewCalorie.setText(textViewCalorie.getText() + " " + mProd.getCalorie());
        textViewGrad.setText(textViewGrad.getText()+" "+mProd.getGrad());
        float prezzoPerc = (float)(mProd.getPrezzo()*100)/MAX_PREZZO;
        customPrezzo.setRiempimento(prezzoPerc/100,R.color.colorPrezzo);
        float caloriePerc = (float)(mProd.getCalorie()*100)/MAX_CALORIE;
        customCalorie.setRiempimento(caloriePerc/100,R.color.colorCalorie);
        float gradPerc = (float)(mProd.getGrad()*100)/MAX_GRAD;
        customGrad.setRiempimento(gradPerc/100,R.color.colorGrad);
    }

    public void onAddClick(View v){
        if(addButton.getText().equals("SALVA")){
            MainActivity.dbInstance.insertProd(mProd);
            addButton.setText("Togli dai preferiti");
        }
        else{
            MainActivity.dbInstance.deleteProd(mProd);
            addButton.setText("SALVA");

        }

    }
}