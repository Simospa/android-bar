package com.example.projectbar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper sInstance;
    private static final String DATABASE_NAME = "database_preferiti.db";
    private static final String DATABASE_TABLE = "preferiti";
    private static final int DATABASE_VERSION = 2;
    private static final String SQL_CREATE_TABLE ="CREATE TABLE " +DATABASE_TABLE+"("
            +"ID INTEGER PRIMARY KEY AUTOINCREMENT, "+
            "TITOLO TEXT, " +
            "DESCRIZIONE TEXT, " +
            "IMG_ID TEXT, " +
            "PREZZO LONG, "+
            "CALORIE LONG, "+
            "GRADAZIONE LONG"+
            ");";
    private static final String SQL_CHECK = "SELECT ID FROM "+DATABASE_TABLE+" WHERE TITOLO = ";
    private static final String SQL_DELETE_TABLE ="DROP TABLE IF EXISTS "+DATABASE_TABLE;
    private static final String SELECT_ALL = "SELECT * FROM "+DATABASE_TABLE;
    public static DatabaseHelper getInstance(Context c){
        if(sInstance==null){
            sInstance = new DatabaseHelper(c.getApplicationContext());
        }
        return sInstance;
    }
    private DatabaseHelper(Context c){
        super(c,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db,oldVersion,newVersion);
    }

    public ArrayList<Prodotti> getAllItems(){
        ArrayList<Prodotti> prod = new ArrayList<Prodotti>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT_ALL,null);
        if(cursor.moveToFirst()){
            do{
                Prodotti p = new Prodotti(cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getLong(4),cursor.getLong(5),cursor.getLong(6));
                p.setDbID(cursor.getLong(0));
                prod.add(p);
            }while(cursor.moveToNext());
        }
        db.close();
        return prod;
    }

    public void deleteProd(Prodotti p){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+DATABASE_TABLE+" WHERE TITOLO ="+"'"+p.getNome()+"'");
        db.close();
    }

    public void insertProd(Prodotti p){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TITOLO",p.getNome());
        values.put("DESCRIZIONE",p.getDescrizione());
        values.put("IMG_ID",p.getImage());
        values.put("PREZZO",p.getPrezzo());
        values.put("CALORIE",p.getCalorie());
        values.put("GRADAZIONE",p.getGrad());
        p.setDbID(db.insert(DATABASE_TABLE,null,values));
        db.close();
    }

    public boolean checkIfExists(Prodotti p){
        SQLiteDatabase db = this.getWritableDatabase();
        String mQuery = SQL_CHECK+"'"+p.getNome()+"'";
        Cursor cursor = db.rawQuery(mQuery,null);
        return cursor.moveToFirst();
    }
}
