package com.example.projectbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class profiloActivity extends AppCompatActivity {

    protected EditText emailEditText;
    protected EditText passwordEditText;
    protected Button logInButton;
    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilo);

        mFirebaseAuth = FirebaseAuth.getInstance();
        emailEditText = (EditText)findViewById(R.id.emailTextEditLog);
        passwordEditText = (EditText)findViewById(R.id.passwordTextEditLog);
        logInButton = (Button)findViewById(R.id.accediButton);
    }

    public void onClickRegistratiButton(View v) {
        if (v.getId() == R.id.registrationButton) {
            Intent registratiIntent = new Intent(getBaseContext(), registrationActivity.class);
            startActivity(registratiIntent);
        }
    }

     public void onClickLogin(View v){
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        email = email.trim();
        password = password.trim();

        if(email.equals("")|| password.equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(profiloActivity.this);
            builder.setMessage("INSERIRE EMAIL E PASSWORD")
            .setTitle("ERRORE")
            .setPositiveButton(android.R.string.ok,null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            mFirebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(profiloActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getBaseContext(),"Ti sei loggato con successo",Toast.LENGTH_LONG).show();

                                Intent intentLogin = new Intent(profiloActivity.this,
                                        MainActivity.class );
                                intentLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentLogin);
                            }else{
                                Toast.makeText(getBaseContext(),"Non ti sei loggato con successo",Toast.LENGTH_LONG).show();

                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(profiloActivity.this);
                                builder.setMessage(task.getException().getMessage())
                                        .setTitle("ERRORE")
                                        .setPositiveButton(android.R.string.ok,null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
            }
     }

}
