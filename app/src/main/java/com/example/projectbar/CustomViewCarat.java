package com.example.projectbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;


import androidx.core.content.ContextCompat;

public class CustomViewCarat extends View {
    private Context mContext;
    private float riempimento;
    private int fillColor = R.color.colorPrimary;
    public CustomViewCarat(Context context, AttributeSet attrs ) {
        super(context,attrs);
        mContext = context;
    }

    @Override
    public void onDraw(Canvas canvas){
        Paint mPaint = new Paint();
        mPaint.setColor(ContextCompat.getColor(mContext,R.color.defaultBackgroundCustomView));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setAntiAlias(true);
        int max_size = Math.min(this.getWidth(), this.getHeight());
        mPaint.setStrokeWidth((float)max_size * 0.25F);
        super.onDraw(canvas);
        float pad = mPaint.getStrokeWidth() * 0.6F;
        RectF mRect = new RectF(0,0,0,0);
        mRect.set(0.0F + pad, 0.0F + pad, (float)this.getWidth() - pad, (float)this.getHeight() - pad);
        float startAngle = 0.0F;
        float drawTo = startAngle + this.getPercentuale() * (float)360;
        canvas.drawArc(mRect, 0.0F, 360.0F, false, mPaint);
        mPaint.setColor(ContextCompat.getColor(this.getContext(), fillColor));
        canvas.drawArc(mRect, startAngle, drawTo, false, mPaint);
    }


    private float getPercentuale(){
        return riempimento;
    }

    public void setRiempimento(float value,int color){
        fillColor = color;
        riempimento = value;
        this.invalidate();
        this.requestLayout();
    }

}
