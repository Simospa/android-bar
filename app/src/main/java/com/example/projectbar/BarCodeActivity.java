package com.example.projectbar;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import java.io.IOException;

public class BarCodeActivity extends AppCompatActivity {
    SurfaceView camera;
    private static final int requestCode = 100;
    private boolean isCodeFind;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);
        isCodeFind = false;
    }

    @Override
    public void onStart(){
        super.onStart();
        if(isCodeFind){
            this.finish();
        }
        else{
            camera = (SurfaceView) findViewById(R.id.camera_view);
            createCameraSource();
        }

    }
    private void createCameraSource() {
        BarcodeDetector barDetector = new BarcodeDetector.Builder(this).build();
        if(!barDetector.isOperational()){
            Log.d("Not operational","Not operational");
            return;
        }
        final CameraSource cameraSource = new CameraSource.Builder(this, barDetector).setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1600,1024)
                .build();
        camera.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (ActivityCompat.checkSelfPermission(BarCodeActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermission();
                    Log.d("Not permission","Not permission");
                    return;
                }
                try {
                    Log.d("TAG","Permesso garantito");
                    cameraSource.start(camera.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
        barDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {}

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if(barcodes.size()>0){
                    if(canGo()){
                        String result = barcodes.valueAt(0).displayValue;
                        Log.d("BARCODE",result);
                        Bundle b = new Bundle();
                        b.putString("barcode",result);
                        Intent i = new Intent(getBaseContext(),ActivityCaratteristiche.class);
                        i.putExtras(b);
                        startActivity(i);
                    }
                }

            }
        });
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},requestCode);
    }

    private synchronized boolean canGo(){
        if(!isCodeFind){
            Log.d("BARCODE","FIND");
            isCodeFind = true;
            return true;
        }
        return false;
    }

}