package com.example.projectbar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {
    public static ArrayList<Prodotti> mProdotti;
    public static Context context;
    public static Bitmap onClickBitmap;
    public RecyclerAdapter(Context context, ArrayList<Prodotti> prod){
        mProdotti = prod;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_view_prodotti,parent,false);
        return new Holder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.Holder holder, int position) {
        Prodotti prod = mProdotti.get(position);
        holder.bindProd(prod);
    }

    public int getItemCount(){
        return mProdotti.size();
    }

    public static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView image;
        private TextView nomeProd,descrProd;

        public Holder(View v){
            super(v);
            image = (ImageView) v.findViewById(R.id.imageViewProd);
            nomeProd = (TextView) v.findViewById(R.id.textViewNome);
            descrProd = (TextView) v.findViewById(R.id.textViewDescr);
            v.setOnClickListener(this);
        }

        public void onClick(View v){
            Prodotti p = RecyclerAdapter.mProdotti.get(getAdapterPosition());
            Bundle b = new Bundle();
            b.putStringArray("RecyclerView",p.getArrayToPass());
            RecyclerAdapter.onClickBitmap = p.getBitmap();
            Intent i = new Intent(RecyclerAdapter.context,ActivityCaratteristiche.class);
            i.putExtras(b);
            RecyclerAdapter.context.startActivity(i);
        }

        public void bindProd(Prodotti p){
            if(p.getBitmap()==null){
                downloadImage(p);
            }
            else{
                image.setImageBitmap(p.getBitmap());
            }
            nomeProd.setText(p.getNome());
            descrProd.setText(p.getDescrizione());
        }

        private void downloadImage(final Prodotti p){
            StorageReference storage = FirebaseStorage.getInstance().getReference();
            final long ONE_MEGABYTE = 1024*1024;
            StorageReference storageRef = storage.child(p.getImage());
            storageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitMap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                    image.setImageBitmap(bitMap);
                    p.setBitmap(bitMap);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {}
            });
        }
    }

}
