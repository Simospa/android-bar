package com.example.projectbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ResultProdActivity extends AppCompatActivity{
    public GridLayoutManager mGridLayoutManager;
    public RecyclerView mRecyclerView;
    private RecyclerView.Adapter<RecyclerAdapter.Holder> mAdapter;
    public static HashMap<String,ArrayList<Prodotti>> saveProd = null;
    private String textViewText;
    public static String TAG;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_prod_view);
        Toolbar tool = findViewById(R.id.toolbar);
        tool.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        switchBundle();
        TextView activityTextView = (TextView)findViewById(R.id.prodTextView);
        activityTextView.setText(textViewText);
        if(saveProd == null){
            saveProd = new HashMap<String,ArrayList<Prodotti>>();
        }
        mGridLayoutManager = new GridLayoutManager(this,2);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_prod);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        if(saveProd.get(TAG)==null){
            ArrayList<Prodotti> temp = new ArrayList<Prodotti>();
            saveProd.put(TAG,temp);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(TAG);
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Map<String,Object> value = (Map<String, Object>)dataSnapshot.getValue();
                    assert value != null;
                    Objects.requireNonNull(saveProd.get(TAG)).clear();
                    collectProd(value);
                    Log.d(TAG, "value is: " + value);
                    setAdapter();
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w(TAG, "FAILED TO READ ", databaseError.toException());
                }
            });
        }
        else{
            setAdapter();
        }

        Log.d("FINISH","ACTIVITY FINITA");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void switchBundle(){
        Bundle b  = getIntent().getExtras();
        if(b!=null){
            textViewText = b.getString("key");
            assert textViewText != null;
            switch(textViewText){
                case "CIBI":
                    TAG= "Cibi";
                    break;
                case "DRINK":
                    TAG = "Drink";
                    break;
                case "BEVANDE":
                    TAG = "Bevande";
                    break;
                case "BIRRE":
                    TAG = "Birre";
                    break;
                case "CAFFE'":
                    TAG = "Caffè";
                    break;
                default:
                    TAG = "";
                    break;
            }
        }
    }

    private void collectProd(Map<String,Object> prodotti){
        for(Map.Entry<String,Object>entry:prodotti.entrySet()){
            Map<String,Object> singleProd = (Map<String,Object>)entry.getValue();
            Long prezzo,cal,grad;
            prezzo = (Long)singleProd.get("Prezzo");
            cal = (Long)singleProd.get("Calorie");
            grad = (Long)singleProd.get("Gradazione");
            Objects.requireNonNull(saveProd.get(TAG)).add(new Prodotti((String)singleProd.get("Titolo"),(String)singleProd.get("Descrizione"),(String)singleProd.get("imgId"),prezzo,cal,grad));
        }
    }

    private void setAdapter(){
        mAdapter = new RecyclerAdapter(this,saveProd.get(TAG));
        mRecyclerView.setAdapter(mAdapter);
    }

}

