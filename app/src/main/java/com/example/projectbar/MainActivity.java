package com.example.projectbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout dl;
    private static final String TAG = "MainActivity";
    private String userEmail;
    public static DatabaseHelper dbInstance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar tool = findViewById(R.id.toolbar);
        tool.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tool);
        dl = findViewById(R.id.drawer_layout);
        NavigationView navigation = findViewById(R.id.nav_view);
        navigation.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, dl, tool, R.string.nav_drawer_open, R.string.nav_drawer_close);
        dl.addDrawerListener(toggle);
        toggle.syncState();
        dbInstance = DatabaseHelper.getInstance(this);
    }

    @Override
    public void onStart(){
        super.onStart();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if(user==null){
            loadLogInView();
        }
        else{
            userEmail = user.getEmail();
        }
    }

    private void loadLogInView () {
        Intent intentLoadLogIn = new Intent(this, profiloActivity.class);
        intentLoadLogIn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentLoadLogIn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLoadLogIn);
        finish();
    }




    public void onBackPressed(){
        if(dl.isDrawerOpen(GravityCompat.START)){
            dl.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    public void onClickMenuButtons(View v) {
        Intent intent = new Intent(getBaseContext(), ResultProdActivity.class);
        Bundle b = new Bundle();
        if (v.getId() == R.id.caffeButton) {
            b.putString("key","CAFFE'");
            intent.putExtras(b);
            startActivity(intent);
        }
        else if (v.getId() == R.id.bevandeButton) {
            b.putString("key","BEVANDE");
            intent.putExtras(b);
            startActivity(intent);
        }
       else if (v.getId() == R.id.cibiButton) {
            b.putString("key","CIBI");
            intent.putExtras(b);
            startActivity(intent);
        }
       else if (v.getId() == R.id.drinkButton) {
            b.putString("key","DRINK");
            intent.putExtras(b);
            startActivity(intent);
        }
       else if (v.getId() == R.id.birreButton) {
            b.putString("key","BIRRE");
            intent.putExtras(b);
            startActivity(intent);
        }
       else if (v.getId() == R.id.ScanButton){
            Intent scanIntent = new Intent(getBaseContext(),BarCodeActivity.class);
            startActivity(scanIntent);
        }

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch(menuItem.getItemId()){
            case R.id.profile:
                Bundle b = new Bundle();
                b.putString("key", userEmail != null ? userEmail : "");
                Intent profileInt = new Intent(getBaseContext(),ProflieInformationAct.class);
                profileInt.putExtras(b);
                startActivity(profileInt);
                break;
            case R.id.preferiti:
                Intent preferitiInt = new Intent(getBaseContext(),preferitiActivity.class);
                startActivity(preferitiInt);
                break;
            case R.id.action_signout:
                Log.d(TAG, "action SignOut clicked");
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this, profiloActivity.class));
                finish();
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + menuItem.getItemId());
        }
        dl.closeDrawer(GravityCompat.START);
        return true;
    }
}

