package com.example.projectbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class ProflieInformationAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_information);
        Toolbar tool = findViewById(R.id.toolbar);
        tool.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setUserEmail();

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUserEmail(){
        Bundle b = getIntent().getExtras();
        TextView email = (TextView)findViewById(R.id.userEmailView);
        if(b!=null) {
            email.setText(b.getString("key"));
        }
    }

}